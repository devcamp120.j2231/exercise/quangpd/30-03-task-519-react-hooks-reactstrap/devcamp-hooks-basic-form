import BasicForm from './Components/BasicsForm';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';


function App() {
  return (
    <div className='text-center' >
      <BasicForm/>
    </div>
  );
}

export default App;
